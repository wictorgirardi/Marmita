# README
    MARMITA  
    V 1.0

  A aplicação Web "Marmita", tem como principal objetivo substituir a necessidade de um caderno de receitas, permitindo o usuario
  a criação, modificação e manutenção de suas receitas em um site contruido com o framework Bootstrap tornando-se assim intuitivo e de facil uso. Possui 2 niveis de autenticação de usuario, sendo o nivel visitante, onde o usuario pode visualizar as receitas já inclusas no site e se cadastrar no mesmo e o nivel administrador, onde o usuario pode alem de visualizar todas as receitas, criar novas, modificar-las ou às apagar, lembrando que esse ultimo nivel de autenticação só se torna possivel após a criação e login de uma conta. O processo de criação de uma nova receita é rapido e intuitivo, ao clicar no botão de New recipe o usuario é direcionado a uma tela onde é orientado a dar nome, uma descrição e adicionar uma foto a receita, sendo todas essas 3 caracterisicas obrigatorias, após isso o usuario ira encontrar dois paineis onde há a possibilidade de maneira livre de adicionar quantos passos ou ingredientes a receita demandar, em seguida é só confirmar e a receita já estará criada com sucesso!          A  autoria da receita é realizada automaticamente com o email logado!
  Se trata de uma aplicação desenvolvida na linguagem Ruby de versão 2.5.1, e por ser para uso Web, usa Rails de versao 5.2.0.
  Alem das Gems padrões do Ruby on Rails, a aplicação usa as seguintes demais:

  - gem haml V- 5.0.4
  - gem simple_form V- 4.0.1
  - gem paperclip V- 6.0
  - gem bootstrap-sass V- 3.3.7
  - gem cocoon V-  1.2.11
  - gem devise V- 4.4.3
  - gem jquery-rails V- 4.3.1
  - gem masonry-rails V- 0.2.4

  Para a criação de um banco de dados foi utilizada a gem sqlite3.
    Para a execução local dessa aplicação é necessario seguir os seguintes passos:
  - Realizar a clonagem do repositorio remoto por meio do comando '$ git clone https://gitlab.com/wictorgirardi/Marmita.git'
  - Relizar o comando 'bundle install'  (realiza a instalação de todas as gems necessarias para a execução do projeto)
  - rails db:migrate
  - rails db:drop 
  - rails s

    Após esses passos voce já deve possuir acesso a aplicação por meio do seu browser no caminho 'Localhost:3000' !

    Desenvolvido pelos alunos do curso de Orientação a objeto:
    -Wictor Bastos Girardi 17/0047326
    -Lieverton Santos 17/0039251
